# LightFramework

A php framework to work with PSR-7 requests and responses.

## How does it work?

In short, the framework recieves a request, determines and calls the proper controller and returns the result as a response.

## Getting Started

### Installation

Install the LightFramework via composer into your projects directory:
```
composer create-project dariorieke/lightframework --stability=dev
```
> If your installation is in a subfolder, you have to edit the .htacces file

### File structure
After installation you should see the following structure inside your project:
```
📦your-project
 ┣ 📂.git
 ┣ 📂config
 ┣ 📂public
 ┃  ┕📜index.php
 ┣ 📂src
 ┣ 📂tests
 ┣ 📂vendor
 ┣ 📜.gitignore
 ┣ 📜.htaccess
 ┣ 📜composer.json
 ┗ 📜composer.lock
```
The `config/` directory contains the framework and applications services and configuration.

`public/index.php` is the main entry point for your application. All requests should be redirected to this file. This is done by default by the `.htaccess` file. The `public/` directory can be used to deliver files like assets directly as it can be accessed from the browser.

`src/` contains the framework files.

`tests/` is the place to put your application tests, there are already some tests for the components in `src/`.

The `vendor/` folder contains all composer dependencies for this project defined in `composer.json` / `composer.lock`.

`.git/` and `.gitignore` are there in case you want to manage your project with git.
___
You might want to ask yourself where to put your applications classes and files. Its up to you. But LightFramework recommends to create a `app/` folder in the root of your application and add autoloading for it to the `composer.json` file like this:
```json
"autoload": {
    "psr-4": {
        "DarioRieke\\LightFramework\\": "src/",
        "MyGreatApplication\\": "app/"
    }
}
``` 
___
### Configuration
To configure the application, you can use .ini files based on the environment. 

Open the `config/config.ini` file. This is the main configuration file. It will be loaded in each environment. 
When you defined a custom configuration option you can access it in the application via the container:
```php 
$container->getParameter('APP_DEBUG');
```
or in the `$_SERVER` superglobal 
```php
$_SERVER['APP_DEBUG'];
``` 

The `config/config.ini` file contains 2 parameters by default:
```ini
APP_ENV = dev
APP_DEBUG = true
```
APP_ENV is the environment the app is running in. The application will load its configuration based on the environment. If you set the environment to - for example -  `'production'`, the framework would attempt to load a `config/production/config.ini` file too which config values would overwrite the previously loaded ones. This way you can easily use different configuration in different environments.

APP_DEBUG controls if your application is in debug mode. 

### Services Configuration
Open the `config/services.php` file to see all default services.

Registering a new service in your application is just as easy as this:
```php
$container->singleton(MyCustomService::class, function ($container) {
	return new MyCustomService();
});
```
As you can see the register function gets the container instance as an argument, so we can easily refer to other services and configuration values like this:
```php
$container->singleton(MyCustomService::class, function ($container) {
	return new MyCustomService(
        $container->getParameter('APP_DEBUG'),
        $container->get(MyOtherDependency::class)
    );
});
```
Just like the `config/config.ini` file, the `services.php` file will be loaded based on the environment. This allows you to overwrite and add services based on the environment. Per default, when testing the application, the `config/test/services.php` file will be loaded.

(Note: Environment based service files must return a callable which recieves the container as an argument, see `config/test/services.php`)

Using services inside your controller is easy. Just typehint the dependency and LightFramework will inject it for you. 
To use the previously defined service inside the controller:

```php
function myController(MyCustomService $service) {
// ...
}
```
You can even use config parameters inside your controller, just use the name of the parameter you want:  
```php
function myController(bool $APP_DEBUG) {
// ...
}
```

Tipp: If you want to use a class as controller and want to use constructor arguments, register it as a service and the CallableResolver will fetch it from the container when loading your controller.

> For more information, take a look at the containers [documentation](https://gitlab.com/dario.rieke/dependency-injection).


### Route Configuration
Open up the `config/routes.php` file. This is the place where you control your applications routes.
To add a route, use:
```php
$routes->add(
	new Route(
		'/',
		function() {
			return 'Hello World';
		}
	)
);
```

Of course you can also use named parameters for your routes. The framework will inject them as arguments into your controller.
```php
 $routes->add(
	new Route(
		//route with 2 named parameters
		'/{task}/{id}',
		//the controller to call
		function($task, $id) {
            return 'Todo:' . $task, $id;
        }
	)
);
```

Even route files will be loaded based on environment, just like services and .ini configuration.

(Note: Environment based route files must return a callable which recieves the route collection to modify as an argument)

> For more information, take a look at the routers [documentation](https://gitlab.com/dario.rieke/router).


## Writing tests
LightFramework provides you with a php unit test case (`src/LightFrameworkTestCase.php`) to extend which sets up the kernel for your tests in a test environment. 
Details coming soon.

## Full example
coming soon...











___



### container parameters/dependencies: 
RouteCollectionInterface::class - stores all basic application routes 
APP_ENV - environment, the app is running in
: 
### dependencies:
ServerRequestFactoryInterface::class - needed for testing with the test suite

next thing: nr 1 from list



## Roadmap / Todo
1. add documentation 
2. write tests for front controller integration / full request cycle test (and config and route loading - but hardly testable) 
3. Maybe create an own PSR-7 implementation
4. in future, separate interfaces from implementations in all components, this should not have a deep impact and should only affect composer.json files 
5. services.php and services.env.php should have the same format!!
 (currently the default services and routes instanciate the route collection and container itself, and return it, while the environment specific files return a callable which uses the container/route collection) - currently not possible due to the order stuff is loaded, but isnt actually a big problem either
 6. add more tests to exception and view listener
 7. add a response emitter to output the response, test it
 8. add default messages to kernel http exceptions
 9. all config files should maybe be example files - add another services file to load own services because on composer update those files will be overwritten
