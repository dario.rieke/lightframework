<?php
namespace DarioRieke\LightFramework;

use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Nyholm\Psr7\Factory\Psr17Factory;

/** 
 * callable which registers test dependencies 
 */
return function(DependencyInjectionContainerInterface $container) {
    /**
     * server request factory for testing 
     */
    $container->singleton(ServerRequestFactoryInterface::class, function() {
        return new Psr17Factory();
    });
};