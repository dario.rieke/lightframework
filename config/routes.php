<?php 
namespace DarioRieke\LightFramework;

use DarioRieke\Router\RouteCollection;
use DarioRieke\Router\Route;


$routes = new RouteCollection();

/**
 * register a GET route with 2 parameters (with regex patterns) which get passed to the controller and the request object
 */
$routes->add(
	new Route(
		'/',
		function() {
			return 'Hello World';
		}
	)
);

 $routes->add(
	new Route(
		//route with 2 named parameters
		'/{task}/{id}',
		//the controller to call
		['App\Controller\ExampleController', 'foo'],
		//regex for the named parameters 
		[
			'task' => '[a-z]+',
			'id'   => '\d+'
		]
	)
);


return $routes;