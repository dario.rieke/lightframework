<?php 
namespace DarioRieke\LightFramework;


use DarioRieke\DependencyInjection\DependencyInjectionContainer;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\EventDispatcher\EventDispatcher;
use DarioRieke\EventDispatcher\EventDispatcherInterface;
use DarioRieke\CallableResolver\CallableResolver;
use DarioRieke\CallableResolver\CallableResolverInterface;
use DarioRieke\CallableResolver\ArgumentResolver;
use DarioRieke\CallableResolver\ArgumentResolverInterface;
use DarioRieke\CallableResolver\ArgumentResolver\RequestArgumentProvider;
use DarioRieke\CallableResolver\ArgumentResolver\RequestAttributeArgumentProvider;
use DarioRieke\CallableResolver\ArgumentResolver\ServiceArgumentProvider;
use DarioRieke\CallableResolver\ArgumentResolver\ServiceParameterArgumentProvider;
use DarioRieke\Router\Router;
use DarioRieke\Router\RouterInterface;
use DarioRieke\Router\RouteCollectionInterface;
use DarioRieke\LightFramework\AppKernel;
use DarioRieke\LightFramework\AppKernelInterface;
use Nyholm\Psr7Server\ServerRequestCreator;
use Nyholm\Psr7Server\ServerRequestCreatorInterface;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use DarioRieke\LightFramework\EventListener\ExceptionListener;
use DarioRieke\LightFramework\EventListener\ViewListener;
use DarioRieke\LightFramework\EventListener\RouterExceptionListener;



$container = new DependencyInjectionContainer();


//////////////////////////////////////////////////////////
/**
 * necessary kernel services
 */
//////////////////////////////////////////////////////////

/**
 * default debug parameter
 */
$container->setParameter('debug', true);

/**
 * EventDispatcher
 */
$container->singleton(EventDispatcherInterface::class, function ($container) {
	$dispatcher = new EventDispatcher();

	//add default router exception listener
	$dispatcher->addListenerProvider($container->get('routerExceptionListener'));
	// add default exception listener
	$dispatcher->addListenerProvider($container->get('defaultExceptionListener'));
	// add default view listener
	$dispatcher->addListenerProvider($container->get('defaultViewListener'));

	return $dispatcher;
});

/**
 * ControllerResolver
 */
$container->singleton(CallableResolverInterface::class, function ($container) {
	return new CallableResolver($container);
});

/**
 * Router
 */
$container->singleton(RouterInterface::class, function ($container) {
	return new Router($container->getParameter(RouteCollectionInterface::class));
});

/**
 * ArgumentResolver
 */
$container->singleton(ArgumentResolverInterface::class, function ($container) {
	return new ArgumentResolver (
		//add argument resolvers here
		new RequestArgumentProvider(), 
		new RequestAttributeArgumentProvider(), 
		new ServiceArgumentProvider($container),
		new ServiceParameterArgumentProvider($container)
	);
});

/**	
 * Psr 17 ResponseFactory
 */
$container->singleton(ResponseFactoryInterface::class, function($container) {
	return new Psr17Factory();
});
$container->alias(ResponseFactoryInterface::class, 'Psr17Factory');

/**	
 * ServerRequestCreator
 * used to create the current request
 */
$container->singleton(ServerRequestCreatorInterface::class, function($container) {
	return new ServerRequestCreator(
		$container->get('Psr17Factory'), // ServerRequestFactory
		$container->get('Psr17Factory'), // UriFactory
		$container->get('Psr17Factory'), // UploadedFileFactory
		$container->get('Psr17Factory')  // StreamFactory
	);
});

/**
 * ServerRequest
 * the currently handled request
 */
$container->singleton(ServerRequestInterface::class, function($container) {
	return ($container->get(ServerRequestCreatorInterface::class))->fromGlobals();
});

/**
 * AppKernel
 * the kernel which handles all requests
 */
$container->singleton(AppKernelInterface::class, function ($container) {
	$kernel = new AppKernel (
		$container->getParameter('APP_ENV'),
		$container->get(EventDispatcherInterface::class),
		$container->get(RouterInterface::class),
		$container->get(CallableResolverInterface::class),
		$container->get(ArgumentResolverInterface::class)
	);
	$kernel->configureContainer($container);
	$kernel->configureRoutes($container->getParameter(RouteCollectionInterface::class));

	return $kernel;
});



//////////////////////////////////////////////////////////
/**
 * default listeners for kernel events
 */
//////////////////////////////////////////////////////////

/**
 * ExceptionListener
 * 
 * handles all uncaught exceptions 
 */
$container->register('defaultExceptionListener', function ($container) {
	return new ExceptionListener($container->getParameter('APP_DEBUG'), $container->get(ResponseFactoryInterface::class));
});

/**
 * ViewListener
 *
 * transforms controller return value into a response 
 */
$container->register('defaultViewListener', function ($container) {
	return new ViewListener($container->get(ResponseFactoryInterface::class));
});

/**
 * RouterExceptionListener
 *
 * transforms router exception into http exception, 
 * should be called before the default exception listener 
 */
$container->register('routerExceptionListener', function ($container) {
	return new RouterExceptionListener($container->get(ResponseFactoryInterface::class));
});

//////////////////////////////////////////////////////////
/**
 * custom optional services
 */
//////////////////////////////////////////////////////////

// ...




return $container;