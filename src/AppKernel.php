<?php 
namespace DarioRieke\LightFramework;

use DarioRieke\Kernel\Kernel;
use DarioRieke\LightFramework\AppKernelInterface;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\CallableResolver\CallableResolverInterface;
use DarioRieke\CallableResolver\ArgumentResolverInterface;
use DarioRieke\Router\RouterInterface;
use DarioRieke\Router\RouteCollectionInterface;
use DarioRieke\EventDispatcher\EventDispatcherInterface;
use DarioRieke\LightFramework\ConfigurationLoader;

/**
 * AppKernel used to handle PSR-7 requests by returning PSR-7 responses
 */
class AppKernel extends Kernel implements AppKernelInterface {

    /** 
     * @var string
     */
    private $environment;

    /** 
     * @var string
     */
    private $projectDir;

    /**
	 * construct the app kernel 
     * @param string $environment the environment the app is running in
	 * @param EventDispatcherInterface	  $dispatcher 
	 * @param RouterInterface       	  $router 
	 * @param CallableResolverInterface $callableResolver 
	 * @param ArgumentResolverInterface $callableResolver 
	 */
	public function __construct(
        string $environment,
		EventDispatcherInterface $dispatcher,
		RouterInterface $router,
		CallableResolverInterface $callableResolver, 
		ArgumentResolverInterface $argumentResolver
	) {
        $this->environment = $environment;
		parent::__construct($dispatcher, $router, $callableResolver, $argumentResolver);
	}

    public function getEnvironment(): string {
        return $this->environment;
    }

    /** 
     * loads and overwrites default services based on the environment
     * @param DependencyInjectionContainerInterface $container the container to configure
     */
    public function configureContainer(DependencyInjectionContainerInterface $container): void {
        $configurationLoader = new ConfigurationLoader();
        $environmentConfigDir = $this->getProjectDir().DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.$this->getEnvironment();

        //load environment based configuration
        $environmentSpecificConfigFile = $environmentConfigDir.DIRECTORY_SEPARATOR.'config.ini';
        if(is_file($environmentSpecificConfigFile)) {
            $configurationLoader->loadConfigurationFile($environmentSpecificConfigFile);
            $configurationLoader->toServer(true);
            $configurationLoader->toContainer($container, true);
        }

        //load environment based services
        $environmentSpecificServicesFile = $environmentConfigDir.DIRECTORY_SEPARATOR.'services.php';
        if(is_file($environmentSpecificServicesFile)) {
            $serviceRegisterCallable = $this->loadCallableConfigFile($environmentSpecificServicesFile);
            //configure services
            $serviceRegisterCallable($container);
        }
    }

    /** 
     * load environment specific routes
     * @param RouteCollectionInterface $routes the RouteCollection to add routes to  
     */
    public function configureRoutes(RouteCollectionInterface $routes): void {
        $environmentConfigDir = $this->getProjectDir().DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.$this->getEnvironment();
        
        //load environment based routes
        $environmentSpecificRoutesFile = $environmentConfigDir.DIRECTORY_SEPARATOR.'routes.php';
        if(is_file($environmentSpecificRoutesFile)) {
            $routeRegisterCallable = $this->loadCallableConfigFile($environmentSpecificRoutesFile);
            //configure routes
            $routeRegisterCallable($routes);
        }
    }

    /** 
     * load a service or route configuration file and return the callable from the file
     * @param  string    $filename  the file to load
     * @return callable             the callable which configures the container
     * @throws Exception  throws Exception if return value of config file is not a callable
     */
    protected function loadCallableConfigFile(string $filename): callable {
        $serviceRegisterCallable = include $filename;
        if(!\is_callable($serviceRegisterCallable)) {
            throw new \Exception("Service and route definition files (${filename}) must return a callable which configures the services.");
        }
        return $serviceRegisterCallable;
    }

    public function getProjectDir(): string {
        if(!$this->projectDir) {
            $this->projectDir = \dirname(__FILE__, 2);
        } 

        return $this->projectDir;
    }
    

}