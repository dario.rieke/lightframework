<?php 
namespace DarioRieke\LightFramework\EventListener;

use Psr\EventDispatcher\ListenerProviderInterface;
use DarioRieke\Kernel\Event\ExceptionEvent;
use Psr\Http\Message\ResponseFactoryInterface;
use DarioRieke\Kernel\Exception\Http\HttpExceptionInterface;

/**
 * ExceptionListener to handle uncaught exceptions
 * during the kernels lifecycle  
 */
class ExceptionListener implements ListenerProviderInterface {
	
	/**
	 * @var bool
	 */
	private $debug;

    /** 
     * @var ResponseFactoryInterface
     */
	private $responseFactory;
	 
	/**
	 * @param bool $debug 
	 * @param ResponseFactoryInterface $responseFactory 
	 */
	function __construct(bool $debug, ResponseFactoryInterface $responseFactory) {
		$this->debug = $debug;
		$this->responseFactory = $responseFactory;
	}

    public function getListenersForEvent(object $event) : iterable {
        if($event instanceof ExceptionEvent) {
            yield [$this, 'onKernelException'];
        }
        return [];
    }

	/**
	 * handles an uncaught exception from the kernel
	 */
	public function onKernelException(ExceptionEvent $event) {
		$e = $event->getException();

		if($e instanceof HttpExceptionInterface) {
            $event->setResponse($this->responseFactory->createResponse($e->getStatusCode(), $e->getMessage()));
		}
		else {
			if($this->debug) {
                $message = (string) $e;
                $response = $this->responseFactory->createResponse(500, $e->getMessage());
                $response->getBody()->write($message);
				$event->setResponse($response);
			}
			else {
				$event->setResponse($this->responseFactory->createResponse(500, $e->getMessage()));
            }
		}
	}

}