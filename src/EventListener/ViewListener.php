<?php
namespace DarioRieke\LightFramework\EventListener;

use Psr\EventDispatcher\ListenerProviderInterface;
use DarioRieke\Kernel\Event\ViewEvent;
use Psr\Http\Message\ResponseFactoryInterface;

/**
 * ViewListener to transform a controllers return value 
 * into an actual response
 */
class ViewListener implements ListenerProviderInterface {
	
    /** 
     * @var ResponseFactoryInterface
     */
    private $responseFactory; 
    
    /**
	 * 
	 * @param ResponseFactoryInterface $responseFactory 
	 */
	function __construct(ResponseFactoryInterface $responseFactory) {
		$this->responseFactory = $responseFactory;
	}

    public function getListenersForEvent(object $event) : iterable {
        if($event instanceof ViewEvent) {
            yield [$this, 'onKernelView'];
        }
        return [];
    }

	/**
	 * turns a controller return value into a response
	 */
	public function onKernelView(ViewEvent $event) {
		$value = $event->getControllerValue();
        $request = $event->getRequest();
        $response = $this->responseFactory->createResponse();
        $stringRepresentation = $this->toString($value);
        
        if($stringRepresentation !== null) {
            $response->getBody()->write($stringRepresentation);
            $event->setResponse($response);
        }
    }
    
        
    /**
     * helper to turn the controller return value into a string
     *
     * @param  mixed $value
     * @return string|null
     */
    private function toString($value): ?string {
        if(\is_string($value) || \is_numeric($value)) return $value;
        elseif(\is_object($value) && method_exists($value, '__toString')) return (string) $value;
        elseif(\is_iterable($value) || \is_object($value)) return print_r($value, true);
        else return null;
    }

}
 ?>