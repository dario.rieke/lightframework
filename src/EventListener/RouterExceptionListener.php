<?php 
namespace DarioRieke\LightFramework\EventListener;

use Psr\EventDispatcher\ListenerProviderInterface;
use DarioRieke\Kernel\Event\ExceptionEvent;
use Psr\Http\Message\ResponseFactoryInterface;
use DarioRieke\Router\Exception\RouterExceptionInterface;
use DarioRieke\Router\Exception\MethodNotAllowedException;
use DarioRieke\Router\Exception\NotFoundException;
use DarioRieke\Kernel\Exception\Http\NotFoundException as HttpNotFoundException;
use DarioRieke\Kernel\Exception\Http\MethodNotAllowedException as HttpMethodNotAllowedException;

/**
 * Listener to transform Router Exception into Http Exception
 */
class RouterExceptionListener implements ListenerProviderInterface {
	
	/**
	 * @var bool
	 */
	private $debug;

    /** 
     * @var ResponseFactoryInterface
     */
	private $responseFactory;
	 
	/**
	 * @param ResponseFactoryInterface $responseFactory 
	 */
	function __construct(ResponseFactoryInterface $responseFactory) {
		$this->responseFactory = $responseFactory;
	}

    public function getListenersForEvent(object $event) : iterable {
        if($event instanceof ExceptionEvent) {
            yield [$this, 'onRouterException'];
        }
        return [];
    }

	/**
	 * turns router exception into http exception
	 */
	public function onRouterException(ExceptionEvent $event) {
		$exception = $event->getException();

		if($exception instanceof RouterExceptionInterface) {
            if($exception instanceof MethodNotAllowedException) {
                $newException = new HttpMethodNotAllowedException("Method not allowed", $exception);
            } 
            elseif($exception instanceof NotFoundException) {
                $newException = new HttpNotFoundException("Not found", $exception);
            }

            $event->setException($newException ?? $exception);
		}
	}
}