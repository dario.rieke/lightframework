<?php 
namespace DarioRieke\LightFramework;

use DarioRieke\Kernel\KernelInterface;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\Router\RouteCollectionInterface;

/**
 * AppKernelInterface
 */
interface AppKernelInterface extends KernelInterface {

    /**
     * get the environment the App is running in
     * @return string
     */
    public function getEnvironment(): string;

    /** 
     * add services and configuration to the app kernel
     * @param DependencyInjectionContainerInterface $container the container to configure
     */
    public function configureContainer(DependencyInjectionContainerInterface $container): void;

    /**
     * add routes to the app kernel
     * @param RouteCollectionInterface $routes the route collection to configure
     */
    public function configureRoutes(RouteCollectionInterface $routes): void;

    /** 
     * get the directory of the current project
     * @return string
     */
    public function getProjectDir(): string;

}