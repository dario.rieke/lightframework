<?php 
namespace DarioRieke\LightFramework;

use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;

/**
 * Class for loading LightFramework configuration 
 */
class ConfigurationLoader {
    
    /** 
     * @var array
     */
    private $configuration;

    /** 
     * load configuration file and parse it 
     * currently supports only .ini file
     * @param string $filename  the file to load
     * @return array            configuration array 
     */
    public function loadConfigurationFile(string $filename): array {
        $this->configuration = \parse_ini_file($filename);
        return $this->configuration;
    }

    /** 
     * load the configuration into $_SERVER superglobal
     * @param bool $overwrite define if existing values should be overwritten
     */
    public function toServer(bool $overwrite = false ): void {
        if($overwrite) {
            foreach($this->configuration as $key => $value) {
                $_SERVER[$key] = $value;
            }
        }
        else {
            foreach($this->configuration as $key => $value) {
                if(isset($_SERVER[$key])) continue; 
                $_SERVER[$key] = $value;
            }
        }
    }

    /** 
     * add config values to dependency injection container as parameters
     * @param DependencyInjectionContainerInterface $container 
     * @param bool $overwrite define if existing values should be overwritten
     */
    public function toContainer(DependencyInjectionContainerInterface $container, bool $overwrite = false ): void {
        if($overwrite) {
            foreach($this->configuration as $key => $value) { 
                $container->setParameter($key, $value);
            }
        }
        else {
            foreach($this->configuration as $key => $value) {
                if($container->hasParameter($key)) {
                    continue;
                }
                $container->setParameter($key, $value);
            }
        }
    }
}