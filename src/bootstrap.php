<?php

namespace DarioRieke\LightFramework;

use DarioRieke\LightFramework\ConfigurationLoader;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\Router\RouteCollectionInterface;

/**
 * return the configured DI container
 * @return DependencyInjectionContainerInterface $container the configured container containing basic configuration and applications basic routes
 */


/**
 * load autoloader
 */
require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 * load the default configured service container
 */
$container = require dirname(__DIR__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'services.php';

/**
 * load default configuration
 */
$loader = new ConfigurationLoader();
$loader->loadConfigurationFile(dirname(__DIR__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.ini');
$loader->toServer();
$loader->toContainer($container);

/**
 * load default routes into container
 */
$container->setParameter(RouteCollectionInterface::class, require dirname(__DIR__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'routes.php');

return $container;
