<?php
namespace DarioRieke\LightFramework;

use PHPUnit\Framework\TestCase;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\LightFramework\AppKernelInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;

/** 
 * Base test case for all LightFramework related tests
 */
class LightFrameworkTestCase extends TestCase {

    /**
     * the app environment used for testing
     * @var string
     */
    const TEST_ENV = 'test';

    /** 
     * @var DependencyInjectionContainerInterface
     */
    protected static $container;

    /** 
     * @var AppKernelInterface
     */
    protected static $kernel;

    /**
     * @var bool
     */
    protected static $booted = false;

	public static function setUpBeforeClass(): void {
        static::boot();
    }

    /** 
     * load the configured kernel and container
     */
    protected static function boot() {
        if(!self::isBooted()) {
            self::$container = include dirname(__DIR__).DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'bootstrap.php';
            self::$container->setParameter('APP_ENV', self::TEST_ENV);
            $_SERVER['APP_ENV'] = self::TEST_ENV;
            self::$kernel = $container->get(AppKernelInterface::class);

            self::$booted = true;
        }
    }

    /**
     * check if kernel has already been booted
     * @return bool
     */
    protected static function isBooted(): bool {
        return self::$booted;
    }

    /**
     * throw an exception if the kernel has not been booted yet
     */
    protected static function ensureIsBooted() {
        if(!self::$booted) throw new \LogicException("The test suite has not been booted yet."); 
    }

    /**
     * get the configured container
     * @return DependencyInjectionContainerInterface
     */
    public static function getContainer(): DependencyInjectionContainerInterface {
        self::ensureIsBooted();
        return static::$container;
    }

    /**
     * get the configured kernel
     * @return AppKernelInterface
     */
    public static function getKernel(): AppKernelInterface {
        self::ensureIsBooted();
        return static::$kernel;
    }

    /**
     * get the server request factory to create requests
     */
    public function getServerRequestFactory(): ServerRequestFactoryInterface {
        self::ensureIsBooted();
        return static::$container->get(ServerRequestFactoryInterface::class);
    }
}