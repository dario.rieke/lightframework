<?php 

namespace DarioRieke\LightFramework;

use Psr\Http\Message\ServerRequestInterface;
use DarioRieke\LightFramework\AppKernelInterface;

/**
 * bootstrap the kernel and container
 */
$container = require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'bootstrap.php';

/**
 * construct the kernel
 */
$kernel = $container->get(AppKernelInterface::class);

/**
 * handle the request
 */
$response = $kernel->handle($container->get(ServerRequestInterface::class));

/**
 * output the response
 */
$statusLine = sprintf('HTTP/%s %s %s'
        , $response->getProtocolVersion()
        , $response->getStatusCode()
        , $response->getReasonPhrase()
);
header($statusLine, TRUE); 

foreach ($response->getHeaders() as $name => $values) {
    $responseHeader = sprintf('%s: %s'
            , $name
            , $response->getHeaderLine($name)
    );
    header($responseHeader, FALSE); /* The header doesn't replace a previous similar header. */
}

echo $response->getBody();
exit();

?>