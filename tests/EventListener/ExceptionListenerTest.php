<?php
namespace DarioRieke\LightFramework\Tests\EventListener;

use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\ListenerProviderInterface;
use DarioRieke\LightFramework\EventListener\ExceptionListener;
use Nyholm\Psr7\Factory\Psr17Factory;
use DarioRieke\Kernel\Event\ExceptionEvent;

class ExceptionListenerTest extends TestCase {

    private $exceptionListener;

	public function setUp(): void {
        $this->exceptionListener = $this->getExceptionListener();
    }

    protected function getExceptionListener(bool $debug = false) {
        return new ExceptionListener($debug, new Psr17Factory());
    }

    protected function getExceptionEventMock() {
        return $this->createMock(ExceptionEvent::class);
    }

    public function testProvidesListenerForExceptionEvent() {
        $this->assertInstanceOf(ListenerProviderInterface::class, $this->exceptionListener);
        $listeners = $this->exceptionListener->getListenersForEvent($this->getExceptionEventMock());

        //make sure the thrown exception is used in some way and a response is set 
        $event = $this->getExceptionEventMock();
        $event->expects($this->once())->method('getException');
        $event->expects($this->once())->method('setResponse');

        foreach($listeners as $listener) $listener($event);
    }
}