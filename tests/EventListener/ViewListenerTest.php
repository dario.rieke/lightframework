<?php
namespace DarioRieke\LightFramework\Tests\EventListener;

use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\ListenerProviderInterface;
use DarioRieke\LightFramework\EventListener\ViewListener;
use Nyholm\Psr7\Factory\Psr17Factory;
use DarioRieke\Kernel\Event\ViewEvent;

class ViewListenerTest extends TestCase {

    private $viewListener;

	public function setUp(): void {
        $this->viewListener = $this->getViewListener();
    }

    protected function getViewListener(bool $debug = false) {
        return new ViewListener(new Psr17Factory());
    }

    protected function getViewEventMock() {
        return $this->createMock(ViewEvent::class);
    }

    /**
     * @dataProvider getStringableValues
     */
    public function testProvidesListenerForViewEvent($someValue) {
        $this->assertInstanceOf(ListenerProviderInterface::class, $this->viewListener);
        $listeners = $this->viewListener->getListenersForEvent($this->getViewEventMock());

        //make sure the provided value is gathered and a response is set 
        $event = $this->getViewEventMock();
        $event->expects($this->once())->method('getControllerValue')->will($this->returnValue($someValue));
        $event->expects($this->once())->method('setResponse');
        foreach($listeners as $listener) $listener($event);
    }

    public function getStringableValues() {
        return [
            'string' => ['teststring'],
            'iterable'  => [ array(1,4,6,7,8,5)],
            'object' => [ new \stdClass() ],
            'numeric' => [ 12 ],
            'object(implementing __toString)' => [ 
                new class() { 
                    public function __toString() { return '__toString!'; }
                }
            ]
        ];
    }
}