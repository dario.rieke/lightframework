<?php
namespace DarioRieke\LightFramework\Tests\EventListener;

use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\ListenerProviderInterface;
use DarioRieke\LightFramework\EventListener\RouterExceptionListener;
use Nyholm\Psr7\Factory\Psr17Factory;
use DarioRieke\Kernel\Event\ExceptionEvent;
use DarioRieke\Router\Exception\MethodNotAllowedException;
use DarioRieke\Router\Exception\NotFoundException;
use DarioRieke\Kernel\Exception\Http\NotFoundException as HttpNotFoundException;
use DarioRieke\Kernel\Exception\Http\MethodNotAllowedException as HttpMethodNotAllowedException;

class RouterExceptionListenerTest extends TestCase {

    private $exceptionListener;

	public function setUp(): void {
        $this->exceptionListener = $this->getExceptionListener();
    }

    protected function getExceptionListener() {
        return new RouterExceptionListener(new Psr17Factory());
    }

    protected function getExceptionEventMock() {
        return $this->createMock(ExceptionEvent::class);
    }

    public function testProvidesListenerForExceptionEvent() {
        $this->assertInstanceOf(ListenerProviderInterface::class, $this->exceptionListener);
    }

    /**
     * @dataProvider getExceptions
     */
    public function testReplacesRouterException($inputException, $expectedOutputExceptionClass) {
        $listeners = $this->exceptionListener->getListenersForEvent($this->getExceptionEventMock());
        // check if the exception gets replaced
        $event = $this->getExceptionEventMock();
        $event->expects($this->once())->method('getException')->will($this->returnValue($inputException));
        $event->expects($this->once())->method('setException')->with($this->isInstanceOf($expectedOutputExceptionClass));

        foreach($listeners as $listener) $listener($event);
    }

    public function getExceptions() {
        return [
            'MethodNotAllowed' => [new MethodNotAllowedException, HttpMethodNotAllowedException::class],
            'NotFound' => [new NotFoundException, HttpNotFoundException::class]
        ];
    }
}