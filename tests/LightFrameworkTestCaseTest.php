<?php
namespace DarioRieke\LightFramework\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\LightFramework\LightFrameworkTestCase;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\LightFramework\AppKernelInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;

class LightFrameworkTestCaseTest extends TestCase {

    /**
     * @var LightFrameworkTestCase
     */
    private $lightFrameworkTestCase;

	public function setUp(): void {
        $this->lightFrameworkTestCase = new LightFrameworkTestCase();
        $this->lightFrameworkTestCase::setUpBeforeClass();
    }

    public function testExtendsPhpUnitTestCase() {
        $this->assertInstanceOf(TestCase::class, $this->lightFrameworkTestCase);
    }

    public function testSetsContainer() {
        $this->assertInstanceOf(DependencyInjectionContainerInterface::class, $this->lightFrameworkTestCase->getContainer());
    }

    public function testSetsKernel() {
        $this->assertInstanceOf(AppKernelInterface::class, $this->lightFrameworkTestCase->getKernel());
    }

    public function testSetsTestEnvironment() {
        $kernel = $this->lightFrameworkTestCase->getKernel();
        $container = $this->lightFrameworkTestCase->getContainer();

        $expectedEnv = $this->lightFrameworkTestCase::TEST_ENV;

        $this->assertSame($expectedEnv, $kernel->getEnvironment());
        $this->assertSame($expectedEnv, $container->getParameter('APP_ENV'));
        $this->assertSame($expectedEnv, $_SERVER['APP_ENV']);
    }

    public function testReturnsRequestFactory() {
        $this->assertInstanceOf(ServerRequestFactoryInterface::class, $this->lightFrameworkTestCase->getServerRequestFactory());
    }
}