<?php
namespace DarioRieke\LightFramework\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\LightFramework\AppKernel;
use DarioRieke\LightFramework\AppKernelInterface;
use DarioRieke\Router\RouterInterface;
use DarioRieke\EventDispatcher\EventDispatcherInterface;
use DarioRieke\CallableResolver\CallableResolverInterface;
use DarioRieke\CallableResolver\ArgumentResolverInterface;

class AppKernelTest extends TestCase {

    private $kernel;

	public function setUp(): void {
        $this->kernel = $this->getKernel();
    }

    public function getKernel($env = 'environment') {
        return new AppKernel(
            $env,
            $this->createMock(EventDispatcherInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(CallableResolverInterface::class),
            $this->createMock(ArgumentResolverInterface::class),
        );
    }

    public function testCanReturnEnvironment() {
        $this->assertSame(
            'environment',
            $this->kernel->getEnvironment()
        );
    }

    public function testCanReturnProjectDir() {
        $this->assertSame(
            \dirname(__FILE__, 2),
            $this->kernel->getProjectDir()
        );
    }

    public function testLoadsEnvironmentBasedConfiguration() {
        static::markTestSkipped('Hardly unit testable.');
    }

    public function testLoadsEnvironmentBasedRoutes() {
        static::markTestSkipped('Hardly unit testable.');
    }

}