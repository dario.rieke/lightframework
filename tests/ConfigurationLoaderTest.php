<?php
namespace DarioRieke\LightFramework\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\LightFramework\ConfigurationLoader;
use DarioRieke\DependencyInjection\DependencyInjectionContainer;

class ConfigurationLoaderTest extends TestCase {

    private $configurationLoader;

	public function setUp(): void {
        $this->configurationLoader = new ConfigurationLoader();
    }

    private function getTestFile() {
        return __DIR__.DIRECTORY_SEPARATOR.'Fixtures'.DIRECTORY_SEPARATOR.'config.ini';
    }

    private function getTestFileParsed() {
        return parse_ini_file($this->getTestFile());
    }

    public function testLoadsAndParsesConfigFile() {
        $file = $this->getTestFile();
        $data = $this->configurationLoader->loadConfigurationFile($file);
        $this->assertSame($this->getTestFileParsed(), $data);
        return $this->configurationLoader;
    } 

    /**
     * @depends testLoadsAndParsesConfigFile
     */
    public function testSetsServerSuperglobalValues($configurationLoader) {
        //without overwriting
        $_SERVER = [
            'VALUE1' => 'defaultValue',
            'VALUE2' => 'defaultValue',
            'VALUE3' => 'defaultValue'
        ];
        $unchangedServer = $_SERVER;
        $configurationLoader->toServer();
        $config = $this->getTestFileParsed();
        //none of the values should be overwritten
        $this->assertSame($unchangedServer, $_SERVER);

        //with overwriting enabled
        $_SERVER = [
            'VALUE1' => 'anotherValue'
        ];
        $configurationLoader->toServer(true);
        $this->assertSame($this->getTestFileParsed(), $_SERVER);
    }

    /**
     * @depends testLoadsAndParsesConfigFile
     */
    public function testSetsContainerParameters($configurationLoader) {
        //without overwriting
        $container = new DependencyInjectionContainer();
        $container->setParameter('VALUE1', 'defaultValue');
        $configurationLoader->toContainer($container);
        
        foreach($this->getTestFileParsed() as $key => $value) {
            //default VALUE1 should not change 
            if($key === 'VALUE1') {
                $this->assertTrue($container->hasParameter($key));
                $this->assertSame('defaultValue', $container->getParameter($key));      
                continue;  
            }
            $this->assertTrue($container->hasParameter($key));
            $this->assertSame($value, $container->getParameter($key));
        }

        //with overwriting enabled
        $container = new DependencyInjectionContainer();
        $defaults = [
            'VALUE1' => 'defaultValue',
            'VALUE2' => 'defaultValue',
            'VALUE3' => 'defaultValue'
        ];
        foreach($defaults as $key => $value) {
            $container->setParameter($key, $value);
        }
        
        $configurationLoader->toContainer($container, true);
        
        //all values should be overwritten
        foreach($this->getTestFileParsed() as $key => $value) {
            $this->assertTrue($container->hasParameter($key));
            $this->assertSame($value, $container->getParameter($key));
        }
    }
}